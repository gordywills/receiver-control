from .rx_socket import RXClient, RXResponse
from flask import abort


def power_command(client: RXClient, cmd: str, _: str) -> RXResponse:
    match cmd:
        case 'powerOn':
            client.send_command("PO")
            return client.send_command("?P")
        case 'powerOff':
            client.send_command("PF")
            return client.send_command("?P")
        case 'powerStatus':
            return client.send_command("?P")
        case _:
            abort(404)


def _volume_status(client: RXClient):
    mute_status = client.send_command("?M")
    if "MUT0" in mute_status.payload:
        return RXResponse(mute_status.valid, "Mute")
    vol_status = client.send_command("?V")
    vol = ((int(vol_status.payload[3:]) / 2) - 80.5)
    return RXResponse(vol_status.valid, str(vol))


def volume_command(client: RXClient, cmd: str, data: str) -> RXResponse:
    match cmd:
        case 'volumeUp':
            client.send_command("VU")
            return _volume_status(client)
        case 'volumeDown':
            client.send_command("VD")
            return _volume_status(client)
        case 'volumeStatus':
            return _volume_status(client)
        case 'volumeMute':
            client.send_command("MZ")
            return _volume_status(client)
        case 'volumeSet':
            if data is None or float(data) < -80.0 or float(data) > 12.0:
                data = "-30.0"
            converted_vol = str(int((float(data) + 80.5) * 2)).zfill(3)
            client.send_command(f"{converted_vol}VL")
            return _volume_status(client)
        case _:
            abort(404)


def _function_status(client: RXClient):
    function_number = client.send_command("?F")
    if not function_number.valid:
        return function_number
    function_text = client.send_command(f"?RGB{function_number.payload[2:]}")
    if not function_text.valid:
        return function_text
    return RXResponse(function_text.valid, function_number.payload[2:] + '-' + function_text.payload[6:])


def function_command(client: RXClient, cmd: str, data: str) -> RXResponse:
    match cmd:
        case 'functionStatus':
            return _function_status(client)
        case 'functionUp':
            client.send_command("FU")
            return _function_status(client)
        case 'functionDown':
            client.send_command("FD")
            return _function_status(client)
        case 'functionSet':
            client.send_command(f"{data}FN")
            return _function_status(client)
