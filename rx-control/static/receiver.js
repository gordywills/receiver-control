var powerChange = function (result) {
    var powerStatus = $('#powerStatus');
    var powerControl = $('#powerControl');
    var switchable = $('.switchable');
    var unSwitchable = $('.unSwitchable');
    var volumeSlider = $('#volumeSlider');
    powerControl.removeClass('powerBlack');
    if (result.valid) {
        if (result.message === 'PWR0') {
            powerControl.addClass('switchable').removeClass('unSwitchable');
            powerControl.html('<img id="powerIcon" src="resources/15104-illustration-of-a-green-power-button-icon-pv.png" height="65">');
            powerStatus.html("On")
            switchable.prop('disabled', false);
            unSwitchable.prop('disabled', true);
            volumeSlider.slider("enable");
        }
        if (result.message === 'PWR1') {
            powerControl.addClass('unSwitchable').removeClass('switchable');
            powerControl.html('<img id="powerIcon" src="resources/15105-illustration-of-a-red-power-button-icon-pv.png" height="65">');
            powerStatus.html("Off")
            switchable.prop('disabled', true);
            unSwitchable.prop('disabled', false);
            volumeSlider.slider("disable");
        }
    } else {
        powerControl.html('<img id="powerIcon" src="resources/15105-illustration-of-a-red-power-button-icon-pv.png" height="65">');
        powerControl.addClass('unSwitchable').removeClass('switchable');
        switchable.prop('disabled', true);
        unSwitchable.prop('disabled', true);
        volumeSlider.slider("disable");
        $('#noPower').show();
    }
};

var volumeChange = function (result) {
    if (result.valid) {
        var units = "dBi";
        var volumeStatus = $("#volumeStatus");
        var volumeMute = $("#volumeMute");
        var volumeSlider = $('#volumeSlider');
        if (result.message === 'Mute') {
            units = '';
            $("#custom-handle").text(result.message);
            volumeStatus.css('color', 'red');
            volumeMute.css('color', 'red');
            volumeSlider.css('background-color', 'red').css('border-color', 'red');
            volumeMute.html('<img id="muteIcon" src="resources/mute.png" height="50">');
        } else {
            volumeStatus.css('color', 'lime');
            volumeMute.css('color', 'lime');
            volumeSlider.css('background-color', 'lime').css('border-color', 'lime');
            volumeMute.html('<img id="muteIcon" src="resources/unMute.png" height="50">');
            volumeSlider.slider("value", result.message);
        }
        volumeStatus.html(result.message + units);
    }
};

var functionChange = function (result) {
    if (result.valid) {
        var parts = result.message.split('-');
        $('#functionStatus').html(parts[1].trim());
    }
};

function commandAjax(command, data, callback) {
    if (typeof data === 'undefined') {
        data = '';
    }
    return fetch("api", {
        method: "POST",
        body: JSON.stringify({
            command: command,
            data: data
        }),
        headers: new Headers({
            'credential': getCookie('credential'),
            'Content-Type': "application/json; charset=utf-8"
        })
    }).then((response) => {
        if (response.status === 401) {
            location.reload()
        }
        if (response.status === 500) {
            return {
                'valid': true,
                'message': "...Error..."
            }
        }
        return response.json()
    }).then((data) => {
        callback(data);
        return true;
    }).catch(err => {
        console.log(err)
    })
}

var sliderSetup = function () {
    var handle = $("#custom-handle");
    var updateHandleText = function (ui) {
        handle.text(ui.value + 'dBi');
    };
    $('#volumeSlider').slider({
        max: 12.0,
        min: -80.0,
        orientation: "horizontal",
        step: 0.5,
        value: -80.0,
        disabled: true,
        create: function () {
            handle.text($(this).slider("value") + 'dBi');
        },
        change: function (event, ui) {
            updateHandleText(ui);
        },
        slide: function (event, ui) {
            updateHandleText(ui);
        },
        stop: function (event, ui) {
            commandAjax('volumeSet', ui.value, volumeChange)
        }
    });
};

function setup_click_listeners() {
    $('#powerControl').click(function () {
        if ($(this).hasClass('unSwitchable')) {
            commandAjax('powerOn', '', powerChange);
        } else {
            commandAjax('powerOff', '', powerChange);
        }
    });

    $('#volumeMute').click(function () {
        commandAjax('volumeMute', '', volumeChange);
    });

    $('#functionUp').click(function () {
        commandAjax('functionUp', '', functionChange);
    });
    $('#volumeUp').click(function () {
        commandAjax('volumeUp', '', volumeChange);
    });

    $('#volumeDown').click(function () {
        commandAjax('volumeDown', '', volumeChange);
    });
    $('#functionDown').click(function () {
        commandAjax('functionDown', '', functionChange);
    });
}

function get_powerStatus_factory() {
    return new Promise(function (resolve, reject) {
        resolve(commandAjax('powerStatus', '', powerChange))
    })
}

function get_volStatus_factory() {
    return new Promise(function (resolve, reject) {
        resolve(commandAjax('volumeStatus', '', volumeChange))
    })
}

function get_funcStatus_factory() {
    return new Promise(function (resolve, reject) {
        resolve(commandAjax('functionStatus', '', functionChange))
    })
}

function get_statuses(cmd_list) {
    var result = Promise.resolve();
    cmd_list.forEach(function (cmd) {
        result = result.then(cmd)
    });
    return result
}

function start_timers(interval) {
    let sleep = duration => new Promise(resolve => setTimeout(resolve, duration))
    let poll = (promiseFn, duration) => promiseFn().then(sleep(duration).then(() => poll(promiseFn, duration)))
    setTimeout(() => true, 5 * 1000)
    poll(() => get_statuses([
        get_powerStatus_factory,
        get_volStatus_factory,
        get_funcStatus_factory
    ]), interval)
}


function setCookie(cname, cvalue, exhours) {
    const d = new Date();
    d.setTime(d.getTime() + (exhours * 60 * 60 * 1000) - 1000);
    let expires = "expires=" + d.toUTCString();
    document.cookie = cname + "=" + cvalue + ";" + expires + ";path=/";
}

function unsetCookie(cname, cvalue) {
    setCookie(cname, cvalue, -1)
    location.reload()
}

function getCookie(cname) {
    let name = cname + "=";
    let ca = document.cookie.split(';');
    for (let i = 0; i < ca.length; i++) {
        let c = ca[i];
        while (c.charAt(0) === ' ') {
            c = c.substring(1);
        }
        if (c.indexOf(name) === 0) {
            return c.substring(name.length, c.length);
        }
    }
    return "";
}

function handle_jwt(response) {
    const payload = response.credential;
    setCookie('credential', payload, 1);
    location.reload();
}

function handle_logout() {
    unsetCookie('credential', getCookie('credential'));
    location.reload();
}