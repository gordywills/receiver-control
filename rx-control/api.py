import json
import socket
from functools import wraps
from os import getenv
from socket import create_connection
from flask import Flask, abort, request, redirect, render_template, url_for, send_from_directory, jsonify
from google.auth.transport import requests
from google.oauth2 import id_token
from .settings import settings
from .rx_socket import RXClient
from .commands import power_command, volume_command, function_command

PREFIX: str = getenv("prefix", default='')
RECEIVER_ADDR = settings["ip"]
RECEIVER_PORT = settings["port"]
RECEIVER_LINE_ENDINGS = settings["line_endings"]
CLIENT_ID: str = getenv("client_id", default=None)
VALID_USERS = json.loads(getenv("valid_users", default=None))


def create_api():
    api = Flask(__name__)
    try:
        client = RXClient(RECEIVER_ADDR, RECEIVER_PORT, RECEIVER_LINE_ENDINGS)
        errtext = None
    except OSError as e:
        client = None
        errtext = str(e)

    def validate_jwt(token):
        try:
            idinfo = id_token.verify_oauth2_token(token, requests.Request(), CLIENT_ID)
            if idinfo['email'] in VALID_USERS:
                return True
            return False
        except ValueError:
            return False

    def validate_jwt_wrapper(f):
        @wraps(f)
        def decorated_function(*args, **kwargs):
            token = request.headers.get('credential')
            if token is not None and validate_jwt(token):
                return f(*args, **kwargs)
            abort(401)

        return decorated_function

    @api.route(PREFIX + '/js/<path:path>')
    @api.route(PREFIX + '/css/<path:path>')
    def send_static(path):
        return send_from_directory('static', path)

    @api.route(PREFIX + '/resources/<path:path>')
    def send_images(path):
        return send_from_directory('resources', path)

    @api.route(PREFIX + '/api', methods=['POST'])
    @validate_jwt_wrapper
    def serve_api():
        payload = request.get_json()
        if client == None:
            abort(523, errtext)
        if payload['command'].startswith("power"):
            return jsonify(power_command(client, payload['command'], payload['data']).get_dict())
        if payload['command'].startswith("volume"):
            return jsonify(volume_command(client, payload['command'], payload['data']).get_dict())
        if payload['command'].startswith("function"):
            return jsonify(function_command(client, payload['command'], payload['data']).get_dict())
        abort(404)

    @api.route(PREFIX + "/", methods=['GET'])
    def home():
        jwt = request.cookies.get('credential')
        if client is None:
            return render_template("offline.html", errtext=errtext)
        if jwt is not None and validate_jwt(jwt):
            return redirect(url_for('rx_control'), code=302)
        return render_template("home.html")

    @api.route(PREFIX + "/control", methods=['GET'])
    def rx_control():
        jwt = request.cookies.get('credential')
        if client is not None and jwt is not None and validate_jwt(jwt):
            return render_template("rx.html")
        return redirect(url_for('home'), code=302)

    @api.route(PREFIX + "/privacy", methods=['GET'])
    @api.route(PREFIX + "/tos", methods=['GET'])
    def privacy_tos():
        return render_template("tos.html")

    return api
