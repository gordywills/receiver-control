from waitress import serve
from .api import create_api

SERVER_PORT = 5000
SERVER = "0.0.0.0"

if __name__ == '__main__':
    api = create_api()
    serve(api, host=SERVER, port=SERVER_PORT)