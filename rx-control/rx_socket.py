import json
import socket
from time import sleep


class RXResponse:
    def __init__(self, valid: bool = False, payload: str = ""):
        self.valid: bool = valid
        self.payload: str = payload

    def is_valid(self):
        self.valid = True

    def is_not_valid(self):
        self.valid = False

    def set_payload(self, message):
        self.payload = message

    def get_payload(self) -> str:
        return self.payload

    def get_dict(self) -> dict:
        return {"valid": self.valid, "message": self.payload}

    def get_json(self) -> str:
        return json.dumps(self.get_dict())

    def __str__(self) -> str:
        return self.get_json()


class RXClient:
    DEFAULT_TIMEOUT = 10.0
    RECONNECT_TIMEOUT = 10.0
    CLEAR_BUFFER_TIMEOUT = 0.5
    RECEIVE_BLOCK_LENGTH = 4096

    def __init__(self, host: str, port: int = 23, line_ending: str = "\r\n"):
        self.LINE_ENDING = line_ending
        self.host = host
        self.port = port
        self.sock = socket.create_connection((host, port), timeout=self.DEFAULT_TIMEOUT)
        self.send_command("")

    def __del__(self):
        try:
            if self.sock is not None:
                self.sock.close()
        except AttributeError:
            pass

    def _clear_buffer(self):
        self.sock.settimeout(self.CLEAR_BUFFER_TIMEOUT)
        try:
            while self.sock.recv(self.RECEIVE_BLOCK_LENGTH):
                pass
            return True
        except TimeoutError:
            return True
        except socket.error:
            connected = False
            attempts = 0
            while not connected and attempts <= 5:
                try:
                    self.sock = socket.create_connection((self.host, self.port), timeout=self.DEFAULT_TIMEOUT)
                    connected = True
                except socket.error:
                    attempts += 1
                    sleep(self.RECONNECT_TIMEOUT)
            return False
        finally:
            self.sock.settimeout(self.DEFAULT_TIMEOUT)

    def send_command(self, cmd: str) -> RXResponse:
        if not self._clear_buffer():
            return RXResponse(False, "Cannot Connect to Socket")
        self.sock.sendall((cmd + self.LINE_ENDING).encode())
        return RXResponse(True, self.sock.recv(self.RECEIVE_BLOCK_LENGTH).decode().strip())
