# VSX-930 remote control via telnet interface
A web page built on top of the telnet API described in [rx-control/resources/VSX-1120-K-RS232.PDF](rx-control/resources/VSX-1120-K-RS232.PDF).  This page only impelments the power, volume and function selectors - that's all I needed.  The remaining functionality could be easily implemented in the same style and I would welcome Pull Requests that do so. and a note of thanx to Raymond Julian for his article that gave me the idea [Remote control your Pioneer VSX receiver over telnet](http://raymondjulin.com/2012/07/15/remote-control-your-pioneer-vsx-receiver-over-telnet/).
 
## Installation

1\. Clone the Repo:
 
````bash
$ git clone https://bitbucket.org/gordywills/receiver-control.git
````

2\. run pip Install

````bash
$ pip install - r requirements
````

3\. edit [rx-control/settings.py](rx-control/settings.py) to add the relevant address properties of your receiver.

4\. Run rx-control as a pyhton module.

```bash
$ python -m rx-control
```

5\. Navigate to [http://localhost:5000](http://localhost:5000) and get using, or serve that from behind a web server like nginx.